﻿using CoolParking.BL.Core;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CoolParking.App
{
    internal class Processor : IDisposable
    {
        readonly string _logFile = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        readonly IParkingService _parkingService;
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;

        public Processor()
        {
            _withdrawTimer = new TimerService();
            _withdrawTimer.Interval = Settings.BillingPeriodInSeconds*1000;
            _logTimer = new TimerService();
            _logTimer.Interval = Settings.LogingPeriodInSeconds*1000;
            if (File.Exists(_logFile))
                File.Delete(_logFile);
            _logService = new LogService(_logFile);
            _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);
        }
        public void Dispose()
        {
            _parkingService?.Dispose();
            if (string.IsNullOrEmpty(_logFile) && File.Exists(_logFile))
                File.Delete(_logFile);
        }

        internal void Run()
        {
            while(ProcessMainMenu()) { }
        }

        private bool ProcessMainMenu()
        {
            DisplayMainMenu();
            var x = Console.ReadKey();
            Console.WriteLine(); // перенос курсора после любой клавиши
            switch (x.KeyChar)
            {
                case '0':
                    return false;
                case '1':
                    DisplayCurrentBalance();
                    break;
                case '2':
                    DisplayCurrentEarnings();
                    break;
                case '3':
                    DisplayCurrentPlaces();
                    break;
                case '4':
                    DisplayCurrentTransactions();
                    break;
                case '5':
                    DisplayHistoryTransactions();
                    break;
                case '6':
                    DisplayVehiclesList();
                    break;
                case '7':
                    AddVehicle();
                    break;
                case '8':
                    RemoveVehicle();
                    break;
                case '9':
                    TopUpVehicle();
                    break;
                default:
                    DisplayErrorString("Указанная вами операция отсутствует в списке. Будьте внимательны");
                    break;
            }
            WaitForReturnToMainMenu();
            return true;
        }

        private void DisplayErrorString(string outputString)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(outputString);
            Console.ResetColor();
        }

        private void WaitForReturnToMainMenu()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Нажмите любую клавишу для возврата в меню");
            Console.ResetColor();
            Console.ReadKey();
        }

        private void TopUpVehicle()
        {
            if (_parkingService.GetFreePlaces() == _parkingService.GetCapacity())
            {
                DisplayErrorString("На парковке нет транспортных средств");
                return;
            }
            string newId = ReadVehicleId();
            if (string.IsNullOrEmpty(newId))
                return;
            decimal balance = ReadVehicleBalance();
            if (balance > 0)
            {
                try
                {
                    _parkingService.TopUpVehicle(newId, balance);
                }
                catch (Exception ex)
                {
                    DisplayErrorString("При пополнении баланса ТС возникла ошибка: " + ex.Message);
                }
            }
        }

        private void RemoveVehicle()
        {
            if (_parkingService.GetFreePlaces() == _parkingService.GetCapacity())
            {
                DisplayErrorString("На парковке нет транспортных средств");
                return;
            }
            string newId = ReadVehicleId();
            if (string.IsNullOrEmpty(newId))
                return;
            try
            {
                _parkingService.RemoveVehicle(newId);
            }
            catch (Exception ex)
            {
                DisplayErrorString("При забирании ТС возникла ошибка: " + ex.Message);
            }
        }

        private void AddVehicle()
        {
            if (_parkingService.GetFreePlaces() == 0)
            {
                DisplayErrorString("Нет свободных мест");
                return;
            }
            string newId = ReadVehicleId();
            if (string.IsNullOrEmpty(newId))
                return;
            if (_parkingService.GetVehicles().Any(t => t.Id == newId))
            {
                DisplayErrorString("ТС с таким ID уже стоит на парковке");
                return;
            }
            VehicleType vt = ReadVehicleType();
            decimal balance = ReadVehicleBalance();
            if (balance > 0)
            {
                try
                {
                    _parkingService.AddVehicle(new Vehicle(newId, vt, balance));
                }
                catch (Exception ex)
                {
                    DisplayErrorString("При добавлении ТС возникла ошибка: " + ex.Message);
                }
            }
        }

        private decimal ReadVehicleBalance()
        {
            WriteCaption("Ведите баланс (> 0): ", false);
            string answ = Console.ReadLine();
            decimal balance;
            while (!decimal.TryParse(answ, out balance) || (balance < 0.01M))
            {
                DisplayErrorString("Неверное значение баланса");
                if (!AskYN("Желаете повторить?"))
                {
                    balance = -1;
                    break;
                }
                Console.WriteLine();
                WriteCaption("Ведите баланс (> 0): ", false);
                answ = Console.ReadLine();
            }
            return balance;
        }

        private VehicleType ReadVehicleType()
        {
            WriteCaption("Ведите тип ТС [Легковое - 1, Грузовое - 2, Автобус - 3, Мотоцикл - 4]: ", false);
            string answ = Console.ReadKey().KeyChar.ToString();
            while (!"1234".Contains(answ))
                answ = Console.ReadKey().KeyChar.ToString();
            Console.WriteLine();
            return (VehicleType)(int.Parse(answ)-1);
        }

        private string ReadVehicleId()
        {
            WriteCaption("Введите ID автомобиля [AA-0000-AA]: ", false);
            string newId = Console.ReadLine();
            while (!StringService.CheckIdFormat(newId))
            {
                DisplayErrorString("Неверный формат ID");
                if (!AskYN("Желаете повторить?"))
                {
                    newId = string.Empty;
                    break;
                }
                Console.WriteLine();
                WriteCaption("Введите ID автомобиля [AA-0000-AA]: ", false);
                newId = Console.ReadLine();
            }
            return newId;
        }

        private bool AskYN(string text)
        {
            WriteCaption(text + " [Y/N]", false);
            string answ = Console.ReadKey().KeyChar.ToString().ToUpper();
            while (answ != "N" && answ != "Y")
            {
                answ = Console.ReadKey().KeyChar.ToString().ToUpper();
            }
            return answ == "Y";
        }

        private void DisplayVehiclesList()
        {
            var vs = _parkingService.GetVehicles();
            if (vs.Count == 0)
            {
                WriteCaption("На парковке", false);
                WriteValue(" нет автомобилей", true);
            }
            else
            {
                WriteCaption("На парковке находятся:", true);
                foreach(var v in vs)
                    WriteValue(v.GetLine(), true);
            }
        }

        private void DisplayHistoryTransactions()
        {
            WriteCaption("История транзакций:", true);
            try
            {
                WriteValue(_parkingService.ReadFromLog(), true);
            }
            catch (Exception e)
            {
                DisplayErrorString(e.Message);
            }
            
        }

        private void DisplayCurrentTransactions()
        {
            WriteCaption("Текущие платежи:");
            Console.WriteLine();
            var ts = _parkingService.GetLastParkingTransactions().ToArray();
            if (ts.Length == 0)
            {
                WriteValue("Отсутствуют", true);
            }
            else
                foreach (var t in ts)
                {
                    WriteValue(t.GetLine(), true);
                }
        }

        private void DisplayCurrentPlaces()
        {
            WriteCaption("Свободно/занято мест: ");
            WriteValue(_parkingService.GetFreePlaces().ToString() + " / " + (_parkingService.GetCapacity() - _parkingService.GetFreePlaces()).ToString(), true);
        }

        private void DisplayCurrentEarnings()
        {
            WriteCaption("Заработано за текущий период: ");
            WriteValue(_parkingService.GetLastParkingTransactions().Sum(ti => ti.Sum).ToString(), true);
        }

        private void DisplayCurrentBalance()
        {
            WriteCaption("Текущий баланс парковки: ");
            WriteValue(_parkingService.GetBalance().ToString(), true);
        }

        private void WriteValue(string outputValue, bool newLine = true)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(outputValue);
            Console.ResetColor();
            if (newLine)
                Console.WriteLine();
        }

        private void WriteCaption(string outputCaption, bool newLine = false)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(outputCaption);
            Console.ResetColor();
            if (newLine)
                Console.WriteLine();
        }

        private void DisplayMainMenu()
        {
            Console.Clear();
            Console.WriteLine("1 - Текущий баланс парковки");
            Console.WriteLine("2 - Заработано за текущий период (до записи в лог)");
            Console.WriteLine("3 - Количество свободных / занятых мест на парковке");
            Console.WriteLine("4 - Транзакции Парковки за текущий период (до записи в лог)");
            Console.WriteLine("5 - История транзакций");
            Console.WriteLine("6 - Список ТС находящихся на Паркинге");
            Console.WriteLine("7 - Поставить ТС на Паркинг");
            Console.WriteLine("8 - Забрать ТС с Паркинга");
            Console.WriteLine("9 - Пополнить баланс ТС");
            Console.WriteLine("==============================================");
            Console.WriteLine("0 - Завершить работу парковки");
            Console.WriteLine("==============================================");
            Console.WriteLine("");
            Console.Write("Выберите операцию:");
        }
    }
}
