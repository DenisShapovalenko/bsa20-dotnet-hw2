﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Core
{
    public static class StringService
    {
        private static Random random = new Random();
        private const string alphaChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string numChars = "0123456789";
        public static string RandomString(int length) => GenerateStringFromCharArray(length, alphaChars);
        
        public static string RandomNumberString(int length) => GenerateStringFromCharArray(length, numChars);

        private static string GenerateStringFromCharArray(int length, string chars) =>
            new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());

        public static bool CheckIdFormat(string id)
        {
            Regex reg = new Regex(@"^[A-Z]{2}-(\d{4})-([A-Z]{2})?");
            return reg.IsMatch(id);
        }

    }
}
