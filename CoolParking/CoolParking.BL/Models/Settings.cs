﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        // Начальный баланс Паркинга - 0;
        public static decimal StartingBalance { get; set; }
        //Вместимость Паркинга - 10;
        public static int ParkingCapacity { get; set; } = 10;
        //Период списания оплаты, N-секунд - 5;
        public static int BillingPeriodInSeconds { get; set; } = 5;
        //Период записи в лог, N-секунд - 60;
        public static int LogingPeriodInSeconds { get; set; } = 60;
        //Тарифы в зависимости от Тр.средства: Легковое - 2, Грузовое - 5, Автобус - 3.5, Мотоцикл - 1;
        public static Dictionary<VehicleType, decimal> Tarifs { get; set; } =
            new Dictionary<VehicleType, decimal>
            {
                { VehicleType.PassengerCar, 2 },
                { VehicleType.Truck, 5 },
                { VehicleType.Bus, 3.5M },
                { VehicleType.Motorcycle, 1 }
            };
        //коэффициент штрафа - 2.5.
        public static decimal PenaltyRatio { get; set; } = 2.5M;
    }
}