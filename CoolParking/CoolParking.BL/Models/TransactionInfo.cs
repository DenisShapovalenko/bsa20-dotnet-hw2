﻿using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public DateTime OperationTime { get; private set; }
        public string VehicleId { get; private set; }
        public decimal Sum { get; private set; }

        public TransactionInfo(DateTime operationTime, string vehicleId, decimal sum)
        {
            OperationTime = operationTime;
            VehicleId = vehicleId;
            Sum = sum;
        }

        public string GetLine() => $"{OperationTime:yyyy-MM-dd HH:mm:ss} {VehicleId} {Sum}";
    }
}