﻿using CoolParking.BL.Core;
using System;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private readonly string _id;
        private readonly VehicleType _vehicleType;
        

        public string Id => _id;
        public VehicleType VehicleType => _vehicleType;
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal startBalance)
        {
            if (startBalance < 0)
                throw new ArgumentException("The balance should be positive", "startBalance");
            if (!StringService.CheckIdFormat(id))
                throw new ArgumentException("Wrong ID format", "id");
            _id = id;
            _vehicleType = vehicleType;
            Balance = startBalance;
        }

        
        public static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder randomIdBuilder = new StringBuilder(10);
            randomIdBuilder.Append(StringService.RandomString(2));
            randomIdBuilder.Append('-');
            randomIdBuilder.Append(StringService.RandomNumberString(4));
            randomIdBuilder.Append('-');
            randomIdBuilder.Append(StringService.RandomString(2));

            return randomIdBuilder.ToString();
        }

        public string GetLine() => $"{Id} {VehicleType} {Balance}";
    }
}