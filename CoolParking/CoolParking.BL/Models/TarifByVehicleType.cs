﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public class TarifByVehicleType
    {
        public VehicleType VehicleType { get; set; }
        public Decimal Tarif { get; set; }
    }
}
